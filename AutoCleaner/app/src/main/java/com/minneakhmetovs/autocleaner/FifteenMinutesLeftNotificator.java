package com.minneakhmetovs.autocleaner;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import static android.content.Context.NOTIFICATION_SERVICE;

public class FifteenMinutesLeftNotificator extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent notificationIntent) {



        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setVisibility(Notification.VISIBILITY_PUBLIC)
                        .setContentIntent(pendingIntent)
                        .setCategory(Notification.CATEGORY_ERROR)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setSmallIcon(R.drawable.w128h1281338911651trashcan)
                        .setContentTitle("Осталось 15 минут!")
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText("Через 15 минут удалятся все файлы. Пожалуйста, сохраните нужные Вам данные."))
                        .setPriority(Notification.PRIORITY_MAX);

        Notification notification = builder.build();

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }


}
