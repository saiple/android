package com.minneakhmetovs.autocleaner;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Toast;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SettingsActivity extends AppCompatActivity {

    static boolean telegramBool = false;
    static boolean whatsappBool = false;
    static boolean videosBool = false;
    static boolean audiosBool = false;
    static boolean picturesBool = false;
    static boolean gifsBool = false;
    static boolean docsBool = false;
    static boolean voicenotesBool = false;
    static boolean everyDayBool = false;
    static boolean everyWeekBool = false;
    static boolean everyMonthBool = false;
    static boolean everyYearBool = false;

    private static final int PERMS_REQUEST_CODE = 123;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.settings);

        final CheckBox telegram = findViewById(R.id.telegram);
        final CheckBox whatsapp = findViewById(R.id.whatsapp);
        final CheckBox videos = findViewById(R.id.videos);
        final CheckBox audios = findViewById(R.id.audios);
        final CheckBox pictures = findViewById(R.id.pictures);
        final CheckBox voicenotes = findViewById(R.id.voicenotes);
        final CheckBox gifs = findViewById(R.id.gifs);
        final CheckBox docs = findViewById(R.id.docs);
        final Button save = findViewById(R.id.save);
        final Button delete = findViewById(R.id.delete);
        final RadioButton day = findViewById(R.id.day);
        final RadioButton week = findViewById(R.id.week);
        final RadioButton month = findViewById(R.id.month);
        final RadioButton year = findViewById(R.id.year);

        final File savingData = new File(getFilesDir() + "/saving.txt");

        reader(savingData);

        whatsapp.setChecked(whatsappBool);
        telegram.setChecked(telegramBool);
        pictures.setChecked(picturesBool);
        videos.setChecked(videosBool);
        audios.setChecked(audiosBool);
        gifs.setChecked(gifsBool);
        docs.setChecked(docsBool);
        voicenotes.setChecked(voicenotesBool);
        day.setChecked(everyDayBool);
        week.setChecked(everyWeekBool);
        month.setChecked(everyMonthBool);
        year.setChecked(everyYearBool);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        telegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (telegram.isChecked())
                    if (telegramCheck()) {
                        telegramBool = true;
                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(), "Telegram не установлен", Toast.LENGTH_SHORT);
                        toast.show();
                        telegram.setChecked(false);
                        telegramBool = false;
                    }
                else telegramBool = false;
            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (whatsapp.isChecked())
                    if (whatsappCheck()) {
                        whatsappBool = true;

                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(), "Whatsapp не установлен", Toast.LENGTH_SHORT);
                        toast.show();
                        whatsapp.setChecked(false);
                        whatsappBool = false;
                    }
                else whatsappBool = false;
            }
        });
//

//

        videos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videos.isChecked())
                    videosBool = true;
                else videosBool = false;
            }
        });


        audios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (audios.isChecked())
                    audiosBool = true;
                else audiosBool = false;
            }
        });

        pictures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pictures.isChecked())
                    picturesBool = true;
                else picturesBool = false;
            }
        });

        gifs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gifs.isChecked())
                    gifsBool = true;
                else gifsBool = false;
            }
        });

        docs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (docs.isChecked())
                    docsBool = true;
                else docsBool = false;
            }
        });
        voicenotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (voicenotes.isChecked())
                    voicenotesBool = true;
                else voicenotesBool = false;
            }
        });

        day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                everyDayBool = true;
                everyWeekBool = false;
                everyMonthBool = false;
                everyYearBool = false;
            }
        });
        week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                everyDayBool = false;
                everyWeekBool = true;
                everyMonthBool = false;
                everyYearBool = false;
            }
        });

        month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                everyDayBool = false;
                everyWeekBool = false;
                everyMonthBool = true;
                everyYearBool = false;
            }
        });

        year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                everyDayBool = false;
                everyWeekBool = false;
                everyMonthBool = false;
                everyYearBool = true;
            }
        });

        save.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                if (!(telegramBool | whatsappBool)) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Выберите мессенджер", Toast.LENGTH_SHORT);
                    toast.show();
                }
                if (!(videosBool | audiosBool | picturesBool | gifsBool | docsBool | voicenotesBool)) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Выберите что удалять", Toast.LENGTH_SHORT);
                    toast.show();
                }
                if (!(everyDayBool | everyWeekBool | everyMonthBool | everyYearBool)) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Выберите время удаления", Toast.LENGTH_SHORT);
                    toast.show();
                }
                if ((telegramBool | whatsappBool) && (videosBool | audiosBool | picturesBool | gifsBool | docsBool | voicenotesBool) && (everyDayBool | everyWeekBool | everyMonthBool | everyYearBool)) {
                    writeToFile(telegramBool, whatsappBool, videosBool, audiosBool, picturesBool, gifsBool, docsBool, voicenotesBool,
                            everyDayBool, everyWeekBool, everyMonthBool, everyYearBool);
                    Toast toast = Toast.makeText(getApplicationContext(), "Настройки сохранены", Toast.LENGTH_SHORT);
                    toast.show();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deleteTest()) {
                    int count = deleting();
                    Toast toast = Toast.makeText(getApplicationContext(), "Удалено " + String.valueOf(count) + " файлов", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    requestPerms();
                }
            }
        });
    }

    static boolean whatsappCheck() {
        File file = new File("storage/emulated/0/WhatsApp/Media");
        return file.exists();
    }

    static boolean telegramCheck() {
        File file = new File("storage/emulated/0/Telegram");
        return file.exists();
    }

    File saving;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    void writeToFile(boolean telegramBool, boolean whatsappBool, boolean videosBool, boolean audiosBool,
                     boolean picturesBool, boolean gifsBool, boolean docsBool, boolean voicenotesBool,
                     boolean everyDayBool, boolean everyWeekBool, boolean everyMonthBool, boolean everyYearBool) {

        saving = new File(getFilesDir() + "/saving.txt");

        try {
            FileWriter fstream1 = new FileWriter(saving);// конструктор с одним параметром - для перезаписи
            BufferedWriter out1 = new BufferedWriter(fstream1); //  создаём буферезированный поток
            out1.write(""); // очищаем, перезаписав поверх пустую строку
            out1.close(); // закрываем
        } catch (Exception e) {

        }

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(saving, true))) {

            writer(whatsappBool, "whatsapp", bufferedWriter);
            writer(telegramBool, "telegram", bufferedWriter);
            writer(picturesBool, "pictures", bufferedWriter);
            writer(videosBool, "videos", bufferedWriter);
            writer(audiosBool, "audios", bufferedWriter);
            writer(gifsBool, "gifs", bufferedWriter);
            writer(docsBool, "docs", bufferedWriter);
            writer(voicenotesBool, "voicenotes", bufferedWriter);
            writer(everyDayBool, "everyday", bufferedWriter);
            writer(everyWeekBool, "everyweek", bufferedWriter);
            writer(everyMonthBool, "everymonth", bufferedWriter);
            writer(everyYearBool, "everyyear", bufferedWriter);
            bufferedWriter.close();

        } catch (IOException e) {

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    void writer(boolean data, String name, BufferedWriter bufferedWriter) {
        try {
            bufferedWriter.write(name + " " + String.valueOf(data) + "\n");
            bufferedWriter.flush();
        } catch (IOException ex) {

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    void reader(File file) {
        if (file != null) {
            Scanner scanner;
            try {
                scanner = new Scanner(file);

                String data = scanner.next();

                if (data.equals("whatsapp")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        whatsappBool = true;
                    else if (data.equals("false"))
                        whatsappBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("telegram")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        telegramBool = true;
                    else if (data.equals("false"))
                        telegramBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("pictures")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        picturesBool = true;
                    else if (data.equals("false"))
                        picturesBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("videos")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        videosBool = true;
                    else if (data.equals("false"))
                        videosBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("audios")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        audiosBool = true;
                    else if (data.equals("false"))
                        audiosBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("gifs")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        gifsBool = true;
                    else if (data.equals("false"))
                        gifsBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();


                data = scanner.next();
                if (data.equals("docs")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        docsBool = true;
                    else if (data.equals("false"))
                        docsBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();


                data = scanner.next();
                if (data.equals("voicenotes")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        voicenotesBool = true;
                    else if (data.equals("false"))
                        voicenotesBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();


                data = scanner.next();
                if (data.equals("everyday")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        everyDayBool = true;
                    else if (data.equals("false"))
                        everyDayBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();


                data = scanner.next();
                if (data.equals("everyweek")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        everyWeekBool = true;
                    else if (data.equals("false"))
                        everyWeekBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("everymonth")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        everyMonthBool = true;
                    else if (data.equals("false"))
                        everyMonthBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("everyyear")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        everyYearBool = true;
                    else if (data.equals("false"))
                        everyYearBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

            } catch (FileNotFoundException e) {
                makingAllFalse();
            } catch (FileDamagedException e) {
                makingAllFalse();
                Toast toast = Toast.makeText(getApplicationContext(), "Файл сохранения поврежден", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    void makingAllFalse() {
        telegramBool = false;
        whatsappBool = false;
        videosBool = false;
        audiosBool = false;
        picturesBool = false;
        gifsBool = false;
        docsBool = false;
        voicenotesBool = false;
        everyDayBool = false;
        everyWeekBool = false;
        everyMonthBool = false;
        everyYearBool = false;
    }

    int deleting() {
        int count = 0;
        if (picturesBool) {
            if (telegramBool)
                count += delete("/Telegram/Telegram Images");
            if (whatsappBool)
                count += delete("/WhatsApp/Media/WhatsApp Images");
        }
        if (videosBool) {
            if (telegramBool)
                count += delete("/Telegram/Telegram Video");
            if (whatsappBool)
                count += delete("/WhatsApp/Media/WhatsApp Video");
        }
        if (audiosBool) {
            if (telegramBool)
                count += delete("/Telegram/Telegram Audio");
            if (whatsappBool)
                count += delete("/WhatsApp/Media/WhatsApp Audio");
        }
        if (gifsBool) {
            if (whatsappBool)
                count += delete("/WhatsApp/Media/WhatsApp Animated Gifs");
        }
        if (docsBool) {
            if (telegramBool)
                count += delete("/Telegram/Telegram Documents");
            if (whatsappBool) {
                count += delete("/WhatsApp/Media/WhatsApp Documents");
            }
        }
        if (voicenotesBool) {
            if (whatsappBool)
                count += delete("/WhatsApp/Media/WhatsApp Voice Notes");
        }
        return count;
    }

    int delete(String path) {
        int count = 0;
        File dir = new File(Environment.getExternalStorageDirectory() + path);
        try {
            File[] file = dir.listFiles();
            for (int j = 0; j < file.length; j++) {
                if (file[j].isFile()) {
                    file[j].delete();
                    count++;
                }
                if (file[j].isDirectory()) {
                    File newFile = new File(file[j].getAbsolutePath());
                    File[] newFileDir = newFile.listFiles();
                    for (int i = 0; i < newFileDir.length; i++) {
                        newFileDir[i].delete();
                        count++;
                    }
                }
            }
        } catch (NullPointerException e){
        }
        return count;
    }

    boolean deleteTest() {
        int res = 0;
        //string array of permissions,
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

        for (String perms : permissions) {
            res = checkCallingOrSelfPermission(perms);
            if (!(res == PackageManager.PERMISSION_GRANTED)) {
                return false;
            }
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean allowed = true;
        switch (requestCode) {
            case PERMS_REQUEST_CODE:
                for (int res : grantResults) {
                    // if user granted all permissions.
                    allowed = allowed && (res == PackageManager.PERMISSION_GRANTED);
                }
                break;
            default:
                // if user not granted permissions.
                allowed = false;
                break;
        }
        if (allowed) {
            //user granted all permissions we can perform our task.
        } else {
            // we will give warning to user that they haven't granted permissions.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Нет доступа к файлам", Toast.LENGTH_SHORT).show();

                } else {
                    MainActivity.mainActivityCreated = false;
                    startActivity(new Intent(getApplicationContext(), NotAllowedActivity.class));
                }
            }
        }
    }

    private void requestPerms() {
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, PERMS_REQUEST_CODE);
        }
    }
}







