package com.minneakhmetovs.autocleaner;

import android.Manifest;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    boolean turnedOn;
    static boolean mainActivityCreated = false;

    private static final int PERMS_REQUEST_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button turnOn = findViewById(R.id.turnOn);
        Button settings = findViewById(R.id.settings);
        Button about = findViewById(R.id.about);
        File file = new File(getFilesDir() + "/saving.txt");

        reader(file);
        turnedOn = readStateAlarm();

        if (turnedOn)
            turnOn.setText(R.string.turnOff);
        else
            turnOn.setText(R.string.turnOn);

        turnOn.setOnClickListener(new View.OnClickListener() {
            Intent dayLeftNotificatorIntent = new Intent(getApplicationContext(), DayLeftNotificator.class);
            PendingIntent dayLeftNotificatorPendingIntent = PendingIntent.getBroadcast(MainActivity.this, 1, dayLeftNotificatorIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent fifteenMinutesLeftNotificatorIntent = new Intent(getApplicationContext(), FifteenMinutesLeftNotificator.class);
            PendingIntent fifteenMinutesLeftNotificatorPendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, fifteenMinutesLeftNotificatorIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent serviceIntent = new Intent(getApplicationContext(), Deleter.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            AlarmManager alarmManagerDayLeft = (AlarmManager) getSystemService(ALARM_SERVICE);
            AlarmManager alarmManagerFifteenLeft = (AlarmManager) getSystemService(ALARM_SERVICE);

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (!turnedOn) {
                    if (deleteTest()) {
                        if (!(SettingsActivity.everyDayBool | SettingsActivity.everyWeekBool | SettingsActivity.everyMonthBool | SettingsActivity.everyYearBool)) {
                            Toast toast = Toast.makeText(getApplicationContext(), "Выберите настройки удаления", Toast.LENGTH_SHORT);
                            toast.show();
                        } else {
                            Date nextDay = setAlarm();
                            turnOn.setText(R.string.turnOff);
                            turnedOn = true;
                            writeStateAlarm(turnedOn);
                            String stringNextDay = nextDay.toString();
                            Toast toast = Toast.makeText(getApplicationContext(), "Планировщик удаления создан. Время следущего удаления: " + stringConverter(stringNextDay) + ".", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    } else {
                        requestPerms();
                    }
                } else {
                    alarmManager.cancel(pendingIntent);
                    alarmManagerDayLeft.cancel(dayLeftNotificatorPendingIntent);
                    alarmManagerFifteenLeft.cancel(fifteenMinutesLeftNotificatorPendingIntent);
                    turnOn.setText(R.string.turnOn);
                    turnedOn = false;
                    writeStateAlarm(turnedOn);
                    Toast toast = Toast.makeText(getApplicationContext(), "Планировщик удален", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public Date setAlarm() {
                Date now = new Date();
                if (SettingsActivity.everyDayBool) {
                    Date dateDay = new Date();
                    dateDay.setDate(dateDay.getDate() + 1);
                    //dateDay.setSeconds(dateDay.getSeconds() + 30);

                    Date fifteenMinutesLeft = new Date();
                    fifteenMinutesLeft.setDate(fifteenMinutesLeft.getDate() + 1);
                    fifteenMinutesLeft.setMinutes(fifteenMinutesLeft.getMinutes() - 15);

                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, dateDay.getTime(), dateDay.getTime() - now.getTime(), pendingIntent);
                    //alarmManagerFifteenLeft.setRepeating(AlarmManager.RTC_WAKEUP, fifteenMinutesLeft.getTime(), fifteenMinutesLeft.getTime() - now.getTime(), fifteenMinutesLeftNotificatorPendingIntent);
                    return dateDay;
                }
                if (SettingsActivity.everyWeekBool) {
                    Date dateWeek = new Date();
                    dateWeek.setDate(dateWeek.getDate() + 7);
                    Date dateDayLeft = new Date();
                    dateDayLeft.setDate(dateDayLeft.getDate() + 6);
                    Date fifteenMinutesLeft = new Date();
                    fifteenMinutesLeft.setDate(fifteenMinutesLeft.getDate() + 7);
                    fifteenMinutesLeft.setMinutes(fifteenMinutesLeft.getMinutes() - 15);
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, dateWeek.getTime(), dateWeek.getTime() - now.getTime(), pendingIntent);
                    alarmManagerDayLeft.setRepeating(AlarmManager.RTC_WAKEUP, dateDayLeft.getTime(), dateDayLeft.getTime() - now.getTime(), dayLeftNotificatorPendingIntent);
                    alarmManagerFifteenLeft.setRepeating(AlarmManager.RTC_WAKEUP, fifteenMinutesLeft.getTime(), fifteenMinutesLeft.getTime() - now.getTime(), fifteenMinutesLeftNotificatorPendingIntent);
                    return dateWeek;
                }
                if (SettingsActivity.everyMonthBool) {
                    Date dateMonth = new Date();
                    dateMonth.setMonth(dateMonth.getMonth() + 1);

                    Date dateDayLeft = new Date();
                    dateDayLeft.setMonth(dateMonth.getMonth() + 1);
                    dateDayLeft.setDate(dateDayLeft.getDate() - 1);

                    Date fifteenMinutesLeft = new Date();
                    fifteenMinutesLeft.setMonth(fifteenMinutesLeft.getMonth() + 1);
                    fifteenMinutesLeft.setMinutes(fifteenMinutesLeft.getMinutes() - 15);
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, dateMonth.getTime(), dateMonth.getTime() - now.getTime(), pendingIntent);
                    alarmManagerDayLeft.setRepeating(AlarmManager.RTC_WAKEUP, dateDayLeft.getTime(), dateDayLeft.getTime() - now.getTime(), dayLeftNotificatorPendingIntent);
                    alarmManagerFifteenLeft.setRepeating(AlarmManager.RTC_WAKEUP, fifteenMinutesLeft.getTime(), fifteenMinutesLeft.getTime() - now.getTime(), fifteenMinutesLeftNotificatorPendingIntent);
                    return dateMonth;
                }
                if (SettingsActivity.everyYearBool) {
                    Date dateYear = new Date();
                    dateYear.setYear(dateYear.getYear() + 1);

                    Date dateDayLeft = new Date();
                    dateDayLeft.setYear(dateYear.getYear() + 1);
                    dateDayLeft.setDate(dateDayLeft.getDate() - 1);

                    Date fifteenMinutesLeft = new Date();
                    fifteenMinutesLeft.setYear(fifteenMinutesLeft.getDate() + 1);
                    fifteenMinutesLeft.setMinutes(fifteenMinutesLeft.getMinutes() - 15);

                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, dateYear.getTime(), dateYear.getTime() - now.getTime(), pendingIntent);
                    alarmManagerDayLeft.setRepeating(AlarmManager.RTC_WAKEUP, dateDayLeft.getTime(), dateDayLeft.getTime() - now.getTime(), dayLeftNotificatorPendingIntent);
                    alarmManagerFifteenLeft.setRepeating(AlarmManager.RTC_WAKEUP, fifteenMinutesLeft.getTime(), fifteenMinutesLeft.getTime() - now.getTime(), fifteenMinutesLeftNotificatorPendingIntent);
                    return dateYear;
                }
                return null;
            }
        });
        settings.setOnClickListener(this);

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), About.class));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    void reader(File file) {
        if (file != null) {
            Scanner scanner;
            try {
                scanner = new Scanner(file);

                String data = scanner.next();

                if (data.equals("whatsapp")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.whatsappBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.whatsappBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("telegram")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.telegramBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.telegramBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("pictures")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.picturesBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.picturesBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("videos")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.videosBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.videosBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("audios")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.audiosBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.audiosBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("gifs")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.gifsBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.gifsBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();


                data = scanner.next();
                if (data.equals("docs")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.docsBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.docsBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();


                data = scanner.next();
                if (data.equals("voicenotes")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.voicenotesBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.voicenotesBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();


                data = scanner.next();
                if (data.equals("everyday")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.everyDayBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.everyDayBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();


                data = scanner.next();
                if (data.equals("everyweek")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.everyWeekBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.everyWeekBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("everymonth")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.everyMonthBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.everyMonthBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

                data = scanner.next();
                if (data.equals("everyyear")) {
                    data = scanner.next();
                    if (data.equals("true"))
                        SettingsActivity.everyYearBool = true;
                    else if (data.equals("false"))
                        SettingsActivity.everyYearBool = false;
                    else throw new FileDamagedException();
                } else throw new FileDamagedException();

            } catch (FileNotFoundException e) {
                makingAllFalse();
            } catch (FileDamagedException e) {
                makingAllFalse();
                Toast toast = Toast.makeText(getApplicationContext(), "Файл сохранения поврежден", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    void makingAllFalse() {
        SettingsActivity.telegramBool = false;
        SettingsActivity.whatsappBool = false;
        SettingsActivity.videosBool = false;
        SettingsActivity.audiosBool = false;
        SettingsActivity.picturesBool = false;
        SettingsActivity.gifsBool = false;
        SettingsActivity.docsBool = false;
        SettingsActivity.voicenotesBool = false;
        SettingsActivity.everyDayBool = false;
        SettingsActivity.everyWeekBool = false;
        SettingsActivity.everyMonthBool = false;
        SettingsActivity.everyYearBool = false;
    }

    boolean readStateAlarm() {
        File file = new File(getFilesDir() + "state.txt");
        try {
            Scanner scanner = new Scanner(file);
            return Boolean.valueOf(scanner.next());
        } catch (IOException e) {
        } catch (NoSuchElementException e) {
        }
        return false;
    }

    void writeStateAlarm(boolean state) {
        try {
            File file = new File(getFilesDir() + "state.txt");
            FileWriter writer = new FileWriter(file);
            writer.write(String.valueOf(state));
            writer.close();
        } catch (IOException e) {
        }
    }

    String stringConverter(String data) {
        String result = "";
        switch (String.valueOf(data.charAt(0)) + data.charAt(1) + data.charAt(2)) {
            case "Mon":
                result += "Понедельник";
                break;
            case "Tue":
                result += "Вторник";
                break;
            case "Wed":
                result += "Среда";
                break;
            case "Thu":
                result += "Четверг";
                break;
            case "Fri":
                result += "Пятница";
                break;
            case "Sat":
                result += "Суббота";
                break;
            case "Sun":
                result += "Воскресенье";
                break;
        }
        result += " ";
        result += String.valueOf(data.charAt(8)) + String.valueOf(data.charAt(9));
        result += " ";
        switch (String.valueOf(data.charAt(4)) + data.charAt(5) + data.charAt(6)) {
            case "Jan":
                result += "Январь";
                break;
            case "Feb":
                result += "Февраль";
                break;
            case "Mar":
                result += "Март";
                break;
            case "Apr":
                result += "Апрель";
                break;
            case "May":
                result += "Май";
                break;
            case "Jun":
                result += "Июнь";
                break;
            case "Jul":
                result += "Июль";
                break;
            case "Aug":
                result += "Август";
                break;
            case "Sep":
                result += "Сентябрь";
                break;
            case "Oct":
                result += "Октябрь";
                break;
            case "Nov":
                result += "Ноябрь";
                break;
            case "Dec":
                result += "Декабрь";
                break;
        }
        result += " ";
        for (int i = 11; i < 19; i++)
            result += String.valueOf(data.charAt(i));
        result += " ";
        for (int i = 30; i < 34; i++)
            result += String.valueOf(data.charAt(i));
        return result;
    }

    boolean deleteTest() {
        int res = 0;
        //string array of permissions,
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

        for (String perms : permissions) {
            res = checkCallingOrSelfPermission(perms);
            if (!(res == PackageManager.PERMISSION_GRANTED)) {
                return false;
            }
        }
        return true;
    }

    private void requestPerms() {
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, PERMS_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean allowed = true;
        switch (requestCode){
            case PERMS_REQUEST_CODE:
                for (int res : grantResults){
                    // if user granted all permissions.
                    allowed = allowed && (res == PackageManager.PERMISSION_GRANTED);
                }
                break;
            default:
                // if user not granted permissions.
                allowed = false;
                break;
        }
        if (allowed){
            //user granted all permissions we can perform our task.
        }
        else {
            // we will give warning to user that they haven't granted permissions.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    Toast.makeText(this, "Нет доступа к файлам", Toast.LENGTH_SHORT).show();

                } else {
                    mainActivityCreated = true;
                    startActivity(new Intent(getApplicationContext(), NotAllowedActivity.class));
                }
            }
        }
    }
}
