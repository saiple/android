package com.minneakhmetovs.autocleaner;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Scanner;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Deleter extends BroadcastReceiver {

    boolean telegramBool;
    boolean whatsappBool;
    boolean videosBool;
    boolean audiosBool;
    boolean picturesBool;
    boolean gifsBool;
    boolean docsBool;
    boolean voicenotesBool;
    boolean everyDayBool;
    boolean everyWeekBool;
    boolean everyMonthBool;
    boolean everyYearBool;


    @RequiresApi
    @Override
    public void onReceive(Context context, Intent notificationIntent) {
        readerConditions(context);
        String text = "Удаление успешно завершено. Удалено " + String.valueOf(deleting()) + " файлов. Время следующего удаления: " + reader(context) + ".";

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setVisibility(Notification.VISIBILITY_PUBLIC)
                        .setContentIntent(pendingIntent)
                        .setCategory(Notification.CATEGORY_ERROR)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setSmallIcon(R.drawable.w128h1281338911651trashcan)
                        .setContentTitle("Удаление завершено.")
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(text))
                        .setPriority(Notification.PRIORITY_MAX);

        Notification notification = builder.build();

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(1, notification);

    }

    String reader(Context context) {
        Date date = new Date();
        readerConditions(context);

        if (everyDayBool)
            date.setDate(date.getDate() + 1);
        if (everyWeekBool)
            date.setDate(date.getDate() + 7);
        if (everyMonthBool)
            date.setMonth(date.getMonth() + 1);
        if (everyYearBool)
            date.setYear(date.getYear() + 1);

        return stringConverter(date.toString());
    }

    String stringConverter(String data) {
        String result = "";
        switch (String.valueOf(data.charAt(0)) + data.charAt(1) + data.charAt(2)) {
            case "Mon":
                result += "Понедельник";
                break;
            case "Tue":
                result += "Вторник";
                break;
            case "Wed":
                result += "Среда";
                break;
            case "Thu":
                result += "Четверг";
                break;
            case "Fri":
                result += "Пятница";
                break;
            case "Sat":
                result += "Суббота";
                break;
            case "Sun":
                result += "Воскресенье";
                break;
        }
        result += " ";
        result += String.valueOf(data.charAt(8)) + String.valueOf(data.charAt(9));
        result += " ";
        switch (String.valueOf(data.charAt(4)) + data.charAt(5) + data.charAt(6)) {
            case "Jan":
                result += "Январь";
                break;
            case "Feb":
                result += "Февраль";
                break;
            case "Mar":
                result += "Март";
                break;
            case "Apr":
                result += "Апрель";
                break;
            case "May":
                result += "Май";
                break;
            case "Jun":
                result += "Июнь";
                break;
            case "Jul":
                result += "Июль";
                break;
            case "Aug":
                result += "Август";
                break;
            case "Sep":
                result += "Сентябрь";
                break;
            case "Oct":
                result += "Октябрь";
                break;
            case "Nov":
                result += "Ноябрь";
                break;
            case "Dec":
                result += "Декабрь";
                break;
        }
        result += " ";
        for (int i = 11; i < 19; i++)
            result += String.valueOf(data.charAt(i));
        result += " ";
        for (int i = 30; i < 34; i++)
            result += String.valueOf(data.charAt(i));
        return result;
    }

    void readerConditions(Context context) {
        File file = new File(context.getFilesDir() + "/saving.txt");

        Scanner scanner;
        try {
            scanner = new Scanner(file);

            String data = scanner.next();

            if (data.equals("whatsapp")) {
                data = scanner.next();
                if (data.equals("true"))
                    whatsappBool = true;
                else if (data.equals("false"))
                    whatsappBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();

            data = scanner.next();
            if (data.equals("telegram")) {
                data = scanner.next();
                if (data.equals("true"))
                    telegramBool = true;
                else if (data.equals("false"))
                    telegramBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();

            data = scanner.next();
            if (data.equals("pictures")) {
                data = scanner.next();
                if (data.equals("true"))
                    picturesBool = true;
                else if (data.equals("false"))
                    picturesBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();

            data = scanner.next();
            if (data.equals("videos")) {
                data = scanner.next();
                if (data.equals("true"))
                    videosBool = true;
                else if (data.equals("false"))
                    videosBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();

            data = scanner.next();
            if (data.equals("audios")) {
                data = scanner.next();
                if (data.equals("true"))
                    audiosBool = true;
                else if (data.equals("false"))
                    audiosBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();

            data = scanner.next();
            if (data.equals("gifs")) {
                data = scanner.next();
                if (data.equals("true"))
                    gifsBool = true;
                else if (data.equals("false"))
                    gifsBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();


            data = scanner.next();
            if (data.equals("docs")) {
                data = scanner.next();
                if (data.equals("true"))
                    docsBool = true;
                else if (data.equals("false"))
                    docsBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();


            data = scanner.next();
            if (data.equals("voicenotes")) {
                data = scanner.next();
                if (data.equals("true"))
                    voicenotesBool = true;
                else if (data.equals("false"))
                    voicenotesBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();


            data = scanner.next();
            if (data.equals("everyday")) {
                data = scanner.next();
                if (data.equals("true"))
                    everyDayBool = true;
                else if (data.equals("false"))
                    everyDayBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();


            data = scanner.next();
            if (data.equals("everyweek")) {
                data = scanner.next();
                if (data.equals("true"))
                    everyWeekBool = true;
                else if (data.equals("false"))
                    everyWeekBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();

            data = scanner.next();
            if (data.equals("everymonth")) {
                data = scanner.next();
                if (data.equals("true"))
                    everyMonthBool = true;
                else if (data.equals("false"))
                    everyMonthBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();

            data = scanner.next();
            if (data.equals("everyyear")) {
                data = scanner.next();
                if (data.equals("true"))
                    everyYearBool = true;
                else if (data.equals("false"))
                    everyYearBool = false;
                else throw new FileDamagedException();
            } else throw new FileDamagedException();

        } catch (FileNotFoundException e) {
        } catch (FileDamagedException e) {
            Toast toast = Toast.makeText(context, "Файл сохранения поврежден", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    int deleting() {
        int count = 0;
        if (picturesBool) {
            if (telegramBool)
                count += delete("/Telegram/Telegram Images");
            if (whatsappBool) {
                count += delete("/WhatsApp/Media/WhatsApp Images");
                System.out.println("deleting...");
            }
        }
        if (videosBool) {
            if (telegramBool)
                count += delete("/Telegram/Telegram Video");
            if (whatsappBool)
                count += delete("/WhatsApp/Media/WhatsApp Video");
        }
        if (audiosBool) {
            if (telegramBool)
                count += delete("/Telegram/Telegram Audio");
            if (whatsappBool)
                count += delete("/WhatsApp/Media/WhatsApp Audio");
        }
        if (gifsBool) {
            if (whatsappBool)
                count += delete("/WhatsApp/Media/WhatsApp Animated Gifs");
        }
        if (docsBool) {
            if (telegramBool)
                count += delete("/Telegram/Telegram Documents");
            if (whatsappBool) {
                count += delete("/WhatsApp/Media/WhatsApp Documents");
            }
        }
        if (voicenotesBool) {
            if (whatsappBool)
                count += delete("/WhatsApp/Media/WhatsApp Voice Notes");
        }
        return count;
    }

    int delete(String path) {
        int count = 0;
        File dir = new File(Environment.getExternalStorageDirectory() + path);
        try {
            File[] file = dir.listFiles();
            for (int j = 0; j < file.length; j++) {
                if (file[j].isFile()) {
                    file[j].delete();
                    count++;
                }
                if (file[j].isDirectory()) {
                    File newFile = new File(file[j].getAbsolutePath());
                    File[] newFileDir = newFile.listFiles();
                    for (int i = 0; i < newFileDir.length; i++) {
                        newFileDir[i].delete();
                        count++;
                    }
                }
            }
        } catch (NullPointerException e){
        }
        return count;
    }
    
}
